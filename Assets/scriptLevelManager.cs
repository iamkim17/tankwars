using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;
public class scriptLevelManager : MonoBehaviour {

    public TextMeshProUGUI[] textMeshObject;
    public Button[] levelButtons;
    public Sprite unlockSprite;
    public Sprite lockSprite;
    public int levelToUnlock;
    bool isGameObjectActive;
    int counter;
    private void Awake()
    {
        //numbering text object to 1 - 50
        for (int i = 0; i < textMeshObject.Length; i++)
        {
            counter += 1;
            textMeshObject[i].text = counter.ToString();
        }
    }
    private void Start()
    {
        //saving int value from PlayerPrefs levelReached
        int levelReached = PlayerPrefs.GetInt("levelReached", 1);

        //Array of Buttons of level
        for (int i = 0; i < levelButtons.Length; i++)
        {
            //levels that greater than the levelReached will be in lock state
            //else Levels are in unlock state
            if(i + 1 > levelReached)
            {
                levelButtons[i].interactable = false;
                textMeshObject[i].enabled = false;
                levelButtons[i].image.overrideSprite = lockSprite;
            }
            else
            {
                levelButtons[i].image.overrideSprite = unlockSprite;
            }
        }
    }

    public void CheatLevelUnlock()
    {
        PlayerPrefs.SetInt("levelReached", levelToUnlock);
        PlayerPrefs.Save();
        //int levelReached = PlayerPrefs.GetInt("levelReached");

        //for (int i = 0; i < levelButtons.Length; i++)
        //{
        //    if (i + 1 > levelReached)
        //    {
        //        levelButtons[i].interactable = false;
        //        textMeshObject[i].enabled = false;
        //        levelButtons[i].image.overrideSprite = lockSprite;
        //    }
        //    else
        //    {
        //        levelButtons[i].image.overrideSprite = unlockSprite;
        //    }
        //}
    }
}
