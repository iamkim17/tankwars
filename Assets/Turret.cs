using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using CnControls;

public class Turret : MonoBehaviour 
{

	public float turningSpeed = 100f;
	//private float turretSpeed = 100f;
    public GameObject bulletPrefab;
    public Transform turretBarrel;
    public int fireDelay = 0;
    public int turretFireDelay = 0;
    bool move = false;
	public void Move() {
		move = true;
	}
	public void Unmove() {
		move = false;
	}
	void Update() {
		transform.position = GameObject.Find ("Tank").transform.position;
	}
	void FixedUpdate () 
	{
		
		var x = CnInputManager.GetAxis("HorizontalTurret") * Time.deltaTime;
		var y = CnInputManager.GetAxis("VerticalTurret") * Time.deltaTime;
		//var t = CnInputManager.GetAxis("Rotate") * Time.deltaTime;

		//Vector3 movement = new Vector3(x, y, 0);
		if(move) {
			float rot_z = Mathf.Atan2(y, x) * Mathf.Rad2Deg;
			transform.rotation = Quaternion.Euler(0f, 0f, rot_z - 90);
		}

        if(fireDelay >= 30)
        {
            turretFireDelay = fireDelay;
            Instantiate(bulletPrefab, turretBarrel.position, turretBarrel.rotation);
            fireDelay = 0;
        }
        else { fireDelay++; }

        if (turretFireDelay > 0) { turretFireDelay--; }


    }

}
