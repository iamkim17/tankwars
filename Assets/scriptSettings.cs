using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class scriptSettings : MonoBehaviour {

    public void Back(string loadLevel)
    {
        SceneManager.LoadScene(loadLevel);
    }
}
