using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class scriptEnemyAim : MonoBehaviour {


    public GameObject target; //Game Object target
    public GameObject entity;
    public float lookAtDistance = 25f; //Look at allowed distance
    public float enemyAimSpeed = 5f; //How fast the enemy aim
    public Transform playerTarget; //get transform
    public float distance; //distance from target from current position
    public float enemyDamage; // enemy Damage to the player
    public float attackRange = 15f; // allowed attack range
    public float attackRepeatTime = 1f; //attack delay of enemy
    private float attackTime; 
    public Transform turretBarrel; //transform position of enemy turret barrel
    public float bulletSpeed; // enemy bullet speed
    public float bulletDestroy; // enemy destroy time
    public scriptBulletMovement bullet; //scriptBulletMovement reference
    public static bool canShoot = true; //if enemy can shoot

    //Get enemyDamage and Set enemyDamage and put in GetSetEnemyDamage
    public float GetSetEnemyDamage
    {
        get { return enemyDamage; }
        set { enemyDamage = value; }
    }

    private void Awake()
    {
        //setting target gameobject as the PlayerTank gameObject by searching by Tag.
        target = GameObject.FindGameObjectWithTag("PlayerTank");
        //attackRange = lookAtDistance;
    }

    private void Update()
    {
        //playerTarget transform equals to the target transform.
        playerTarget = target.transform;
        //Calculate Distance from playerTarget.position from this object position.
        distance = Vector3.Distance(playerTarget.position, transform.position);

        //if distance is less than the lookDistance and if canShoot is true
        //LookAt() Method will be called.
        if (distance < lookAtDistance)
        {
            if(canShoot)
            {
                LookAt();
            }
            
        }
        //if distance is less than the attackRange and if canShoot is true
        //AttackTarget() Method will be called.
        if (distance < attackRange)
        {
            if(canShoot)
            {
                AttackTarget();
            }
            
        }
    }

    void LookAt()
    {
        // Get Angle in Radians
        float AngleRad = Mathf.Atan2(target.transform.position.y - entity.transform.position.y, target.transform.position.x - entity.transform.position.x);
        // Get Angle in Degrees
        float AngleDeg = (180 / Mathf.PI) * AngleRad;
        // Rotate Object
        var newRotation = Quaternion.Euler(0, 0, AngleDeg - 90);
        transform.rotation = Quaternion.Lerp(transform.rotation, newRotation, Time.deltaTime * enemyAimSpeed);
    }

    void AttackTarget()
    {
        //Calculate attackTime if Time.time greater than attackTime
        if(Time.time > attackTime)
        {
            
            attackTime = Time.time + attackRepeatTime;
            //Instantiate enemyBullet from the turretBarrel
            scriptBulletMovement enemyBullet = Instantiate(bullet, turretBarrel.position, turretBarrel.rotation) as scriptBulletMovement;
            //assign enemyBullet speed from bulletSpeed
            enemyBullet.speed = bulletSpeed;
            //assign enemyBullet destroy from bulletDestroy
            enemyBullet.destroy = bulletDestroy;
            enemyBullet.target = target;
            //assign enemyBullet enemyDamage from enemyDamage
            enemyBullet.enemyDamage = enemyDamage;
            
        }
    }


}

