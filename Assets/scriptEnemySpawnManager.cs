using System.Collections;
using UnityEngine;

public class scriptEnemySpawnManager : MonoBehaviour {


    public enum Spawnstate { SPAWNING, WAITING, COUNTING };

    [System.Serializable]
    public class Wave
    {
        public string name;
        public Transform[] enemy;
        public int count;
        public float rate;

    }

    public Wave[] waves;
    private int nextWave = 0;


    public float timeBetweenWaves = 5f;
    public float waveCountdown;

    public Transform[] spawnPoints;
    public GameObject youWinPanel;
    private float searchCountdown = 1f;

    private Spawnstate state = Spawnstate.COUNTING;

    void Start()
    {
        if (spawnPoints.Length == 0)
        {
            //Debug.LogError("No spawn points referenced");
        }
        waveCountdown = timeBetweenWaves;
        youWinPanel.SetActive(false);
    }

    void Update()
    {
        if (state == Spawnstate.WAITING)
        {
            if (!EnemyIsAlive())
            {
                WaveCompleted();
            }
            else
            {
                return;
            }
        }

        if (waveCountdown <= 0)

        {
            if (state != Spawnstate.SPAWNING)
            {
                StartCoroutine(SpawnWave(waves[nextWave]));
            }

        }
        else
        {
            waveCountdown -= Time.deltaTime;
        }
    }

    void WaveCompleted()

    {
        //Debug.Log("Wave Completed!");

        state = Spawnstate.COUNTING;
        waveCountdown = timeBetweenWaves;

        if (nextWave + 1 > waves.Length - 1)

        {
            nextWave = 0;
            Debug.Log("Completed all waves!");
            youWinPanel.SetActive(true);
            
        }

        else
        {
            nextWave++;
        }



    }

    bool EnemyIsAlive()
    {
        searchCountdown -= Time.deltaTime;
        if (searchCountdown <= 0f)
        {
            searchCountdown = 1f;
            if (GameObject.FindGameObjectWithTag("EnemyTank") == null)
            {
                return false;
            }
        }
        return true;
    }


    IEnumerator SpawnWave(Wave _wave)
    {
        //Debug.Log("Spawning Wave: " + _wave.name);
        state = Spawnstate.SPAWNING;

        for (int i = 0; i < _wave.count; i++)
        {
            int randNumMin = 0;
            int randNumMax = _wave.enemy.Length;
            int randEnemyNum;
            randEnemyNum = Random.Range(randNumMin, randNumMax);
            SpawnEnemy(_wave.enemy[randEnemyNum]);
            yield return new WaitForSeconds(1f / _wave.rate);
        }

        state = Spawnstate.WAITING;


        yield break;
    }

    void SpawnEnemy(Transform _enemy)

    {
        //Debug.Log("Spawning Enemy: " + _enemy.name);


        Transform _sp = spawnPoints[Random.Range(0, spawnPoints.Length - 1)];
        Instantiate(_enemy, _sp.position, _sp.rotation);

    }

}
