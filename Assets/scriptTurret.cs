using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class scriptTurret : MonoBehaviour {
    public bool isFiring;
    public float turningSpeed = 100f;
    //private float turretSpeed = 100f;
    public scriptBulletMovement bullet;
    public Transform turretBarrel;
    public float timeBetweenShots;
    private float shotCounter;
    public float bulletSpeed;
    public float bulletDestroy;
    public static int hitDamage;
    public int playerHitDamage;
    public GameObject enemyTarget;
    bool move = false;
    int firePowerUpgrade;
    [SerializeField]
    SpriteRenderer playerTurretSprite;
    public Sprite bluePlayerTurret;
    public Sprite redPlayerTurret;
    public Sprite blackPlayerTurret;
    //if move is true player can move turret
    public void Move()
    {
        move = true;
    }
    //if move is false player cannot move
    public void Unmove()
    {
        move = false;
    }
    private void Awake()
    {
        //Assign hitDamage value from playerHitDamage
        hitDamage = playerHitDamage;
        firePowerUpgrade = PlayerPrefs.GetInt(scriptShop.firePowerKey); //fire power key

        //if fire power upgraded equals to 2 then playerHitDamage will increment by 20
        //if (firePowerUpgrade == 1)
        //{
        //    playerHitDamage += 10;
        //}
        if (firePowerUpgrade == 2)
        {
            playerHitDamage += 20;
        }
        if (firePowerUpgrade == 3)
        {
            playerHitDamage += 30;
        }
        if (firePowerUpgrade == 4)
        {
            playerHitDamage += 40;
        }
        if (firePowerUpgrade == 5)
        {
            playerHitDamage += 50;
        }
        if (firePowerUpgrade == 6)
        {
            playerHitDamage += 60;
        }
        if (firePowerUpgrade == 7)
        {
            playerHitDamage += 70;
        }
        if (firePowerUpgrade == 8)
        {
            playerHitDamage += 80;
        }
        if (firePowerUpgrade == 9)
        {
            playerHitDamage += 90;
        }
        if (firePowerUpgrade == 10)
        {
            playerHitDamage += 100;
        }

        //if fire power upgrade from shop is 3 - 5 then player turret sprite will be color blue
        if (firePowerUpgrade >= 3 && firePowerUpgrade <= 5)
        {
            playerTurretSprite.sprite = bluePlayerTurret;
        }
        //if fire power upgrade from shop is 6 - 8 then player turret sprite will be color blue
        if (firePowerUpgrade >= 6 && firePowerUpgrade <= 8)
        {
            playerTurretSprite.sprite = redPlayerTurret;
        }
        //if fire power upgrade from shop is 9 - 10 then player turret sprite will be color blue
        if (firePowerUpgrade >= 9 && firePowerUpgrade <= 10)
        {
            playerTurretSprite.sprite = blackPlayerTurret;
        }
    }

    void Update()
    {
        //find player tank body position and assign turret to the center body
        transform.position = GameObject.FindWithTag("PlayerTank").transform.position;

    }
    void FixedUpdate()
    {
        //Turret Input Control
        var x = ControlFreak2.CF2Input.GetAxis("HorizontalTurret");
        var y = ControlFreak2.CF2Input.GetAxis("VerticalTurret");
        float mag = Mathf.Clamp01(new Vector2(ControlFreak2.CF2Input.GetAxis("HorizontalTurret"), ControlFreak2.CF2Input.GetAxis("VerticalTurret")).magnitude);
        //var t = CnInputManager.GetAxis("Rotate") * Time.deltaTime;
        if (x < 0 || x > 0)
        {
            move = true;
            
            
        }
        else
        {
            move = false;
            
        }
        //Debug.Log(mag);
        //Calculate magnitude of input so that you can control the firing of the player if you touch swipe the
        //input control from the max boundary of input controller
        if (mag >= 1)
        {
            isFiring = true;
        }
        else
        {
            isFiring = false;
        }
        //Movement rotation of turret
        //Vector3 movement = new Vector3(x, y, 0);
        if (move)
        {
            float rot_z = Mathf.Atan2(y, x) * Mathf.Rad2Deg * turningSpeed;
            transform.rotation = Quaternion.Euler(0f, 0f, rot_z - 90);
        }

        //if isFiring is true, shotCounter minus equal to the Time.deltaTime for shot delays
        //then instantiate bullet prefabs then tag as PlayerBullet
        if (isFiring)
        {
            shotCounter -= Time.deltaTime;
            if (shotCounter <= 0f)
            {
                shotCounter = timeBetweenShots;
                scriptBulletMovement newBullet = Instantiate(bullet, turretBarrel.position, turretBarrel.rotation) as scriptBulletMovement;
                newBullet.speed = bulletSpeed;
                newBullet.destroy = bulletDestroy;
                newBullet.tag = "PlayerBullet";
            }
        }
        else
        {
            shotCounter = 0f;
        }


    }
	
}
