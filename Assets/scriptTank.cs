using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
public class scriptTank : MonoBehaviour {

    public float speed = 200f;
    //public float turretSpeed = 100f;
    public int curHealth;
    public int maxHealth = 100;
    public Image Bar;
    int healthUpgrade;
    int speedUpgrade;
    [SerializeField]
    bool move = false;
    public bool isDefPowerUpOn = false;
    public static bool isInviPowerUpOn = false;
    public GameObject playerTankBody;
    public Sprite bluePlayerTankBody;
    public Sprite redPlayerTankBody;
    public Sprite blackPlayerTankBody;
    public GameObject youLosePanel;
    float shieldDuration;
    float invisibleDuration;
    public float shieldTimer;
    public float inviTimer;
    public GameObject shield;
    //if move is true player can move
    public void Move()
    {
        move = true;
    }
    //if move is false player cannot move
    public void Unmove()
    {
        move = false;
    }
    private void Awake()
    {
        shieldTimer = shieldDuration; //shield timer equal to the shield duration
        inviTimer = invisibleDuration; // invisible timer will be equal to invisible duration
        shield.SetActive(false);
        youLosePanel.SetActive(false);
        healthUpgrade = PlayerPrefs.GetInt(scriptShop.healthKey); // get data for shop health upgrade
        speedUpgrade = PlayerPrefs.GetInt(scriptShop.speedKey); // get data for shop speed upgrade


        //Health Upgrade if health upgrade is upgraded to 2 then increment by 20
        if (healthUpgrade == 2)
        {
            maxHealth += 20;
        }
        if (healthUpgrade == 3)
        {
            maxHealth += 30;
        }
        if (healthUpgrade == 4)
        {
            maxHealth += 40;
            
        }
        if (healthUpgrade == 5)
        {
            maxHealth += 50;
        }
        if (healthUpgrade == 6)
        {
            maxHealth += 60;
        }
        if (healthUpgrade == 7)
        {
            maxHealth += 70;
        }
        if (healthUpgrade == 8)
        {
            maxHealth += 80;
        }
        if (healthUpgrade == 9)
        {
            maxHealth += 90;
        }
        if (healthUpgrade == 10)
        {
            maxHealth += 100;
        }


        //Speed Upgrade if speed upgrade is upgraded to 2 then increment by 20
        //if (speedUpgrade == 1)
        //{
        //    speed += 10;
        //}
        if (speedUpgrade == 2)
        {
            speed += 20;
        }
        if (speedUpgrade == 3)
        {
            speed += 30;
        }
        if (speedUpgrade == 4)
        {
            speed += 40;
        }
        if (speedUpgrade == 5)
        {
            speed += 50;
        }
        if (speedUpgrade == 6)
        {
            speed += 60;
        }
        if (speedUpgrade == 7)
        {
            speed += 70;
        }
        if (speedUpgrade == 8)
        {
            speed += 80;
        }
        if (speedUpgrade == 9)
        {
            speed += 90;
        }
        if (speedUpgrade == 10)
        {
            speed += 100;
        }

        //if we upgraded our health to 3 - 5  or speed to 3 - 5 then wi get the blue tank body
        if(healthUpgrade >= 3 && healthUpgrade <= 5 || speedUpgrade >= 3 && speedUpgrade <= 5)
        {
            playerTankBody.GetComponent<SpriteRenderer>().sprite = bluePlayerTankBody;
        }
        //if we upgraded our health to 6 - 8  or speed to 6 - 6 then wi get the red tank body
        if (healthUpgrade >= 6 && healthUpgrade <= 8 || speedUpgrade >= 6 && speedUpgrade <= 8)
        {
            playerTankBody.GetComponent<SpriteRenderer>().sprite = redPlayerTankBody;
        }
        //if we upgraded our health to 9 - 10  or speed to 9 - 10 then wi get the black tank body
        if (healthUpgrade >= 9 && healthUpgrade <= 10 || speedUpgrade >= 9 && speedUpgrade <= 10)
        {
            playerTankBody.GetComponent<SpriteRenderer>().sprite = blackPlayerTankBody;
        }
    }
    private void Start()
    {
        curHealth = maxHealth;
    }
    void FixedUpdate()
    {
        //player input
        var x = ControlFreak2.CF2Input.GetAxis("Horizontal") * Time.deltaTime;
        var y = ControlFreak2.CF2Input.GetAxis("Vertical") * Time.deltaTime;
        // if player input x is less than 0 or x > 0
        // then we can move the player else we cannot move the player
        if(x < 0 || x > 0)
        {
            move = true;
        }
        else
        {
            move = false;
        }
        //vector movement depends on input
        Vector3 movement = new Vector3(x, y, 0);
        //move is true then we can control the rotation by calculating is transform rotation using Atan2 and convertion of rad to degree
        if (move)
        {
            float rot_z = Mathf.Atan2(y, x) * Mathf.Rad2Deg;
            transform.rotation = Quaternion.Euler(0f, 0f, rot_z - 90);
        }

        //movement of player by translating its position
        transform.Translate(movement * speed * Time.deltaTime, Space.World);
        //health method called
        Health();
        //PowerUpActivate called
        PowerUpActivate();

    }

    void Health()
    {
        //check if current health is less than 0 then call the method die
        if(curHealth <= 0)
        {
            Die();
        }
        //calculation for health display
        float calc_health = (float)curHealth / (float)maxHealth;
        SetHealth(calc_health);
    }

    void Die()
    {
        //Lose Panel object activate when died
        youLosePanel.SetActive(true);
    }

    //damage recieve from enemies
    public void TakePlayerDamage(int amount)
    {
        curHealth -= amount;
    }
    //Display health in UI
    void SetHealth(float healthAmount)
    {
        Bar.fillAmount = healthAmount;
    }

    void PowerUpActivate()
    {
        //When Defense Power is On
        if(isDefPowerUpOn)
        {
            
            shieldTimer -= Time.deltaTime;
            if(shieldTimer <= 0)
            {
                shieldTimer = shieldDuration;
                isDefPowerUpOn = false;
                shield.SetActive(false);
            }
            
        }
        //When Invi Power is On
        if(isInviPowerUpOn)
        {
            
            inviTimer -= Time.deltaTime;
            if(inviTimer <= 0)
            {
                inviTimer = invisibleDuration;
                isInviPowerUpOn = false;
            }
            
        }


    }
}
