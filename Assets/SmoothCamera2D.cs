using UnityEngine;
using System.Collections;

public class SmoothCamera2D : MonoBehaviour
{

    public float dampTime = 0.15f;
    private Vector3 velocity = Vector3.zero;
    public Transform target;
    Camera mainCam;
    public Vector2 minimumBoundary;
    public Vector2 maximumBoundary;

    private void Start()
    {
        if (mainCam == null)
            mainCam = Camera.main;


    }

    // Update is called once per frame
    void Update()
    {
        if (target == null)
        {
            target = GameObject.FindWithTag("PlayerTank").transform;

        }
        //target = GameObject.FindGameObjectWithTag("PlayerTank").transform;

        if (target)
        {
            Vector3 point = mainCam.WorldToViewportPoint(target.position);
            Vector3 delta = target.position - mainCam.ViewportToWorldPoint(new Vector3(0.5f, 0.5f, point.z)); //(new Vector3(0.5, 0.5, point.z));
            Vector3 destination = transform.position + delta;
            transform.position = Vector3.SmoothDamp(transform.position, destination, ref velocity, dampTime);
            transform.position = new Vector3(
            Mathf.Clamp(transform.position.x, minimumBoundary.x, maximumBoundary.x),
            Mathf.Clamp(transform.position.y, minimumBoundary.y, maximumBoundary.y),
            transform.position.z
            );
        }

    }
}