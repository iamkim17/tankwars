using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;
using System.Collections;
namespace FadeSceneLevel
{
    public class SceneFader : MonoBehaviour
    {

        public Image img;
        public AnimationCurve curve;
        public int levelToUnlock;
        private int coinWon = 0;
        private int currentCoins = 0;
        private void Awake()
        {
            currentCoins = PlayerPrefs.GetInt(scriptShop.coinsKey);
            
        }
        void Start()
        {
            StartCoroutine(FadeIn());
        }

        public void FadeTo(string scene)
        {
            StartCoroutine(FadeOut(scene));
        }
        public void FadeToWithTimeScale(string scene)
        {
            StartCoroutine(FadeOut(scene));
            Time.timeScale = 1;
        }
        public void FadeToLevelUnlock(string scene)
        {

            PlayerPrefs.SetInt("levelReached", levelToUnlock);
            StartCoroutine(FadeOutWithAddCoin(scene));
        }
        public void FadeToCurrentLevel(string scene)
        {
            StartCoroutine(FadeOutWithAddCoin(scene));
        }
        public void FadeToMainMenu(string scene)
        {
            StartCoroutine(FadeOutWithAddCoin(scene));
        }
        IEnumerator FadeIn()
        {
            float t = 1f;

            while (t > 0f)
            {
                t -= Time.deltaTime;
                float a = curve.Evaluate(t);
                img.color = new Color(0f, 0f, 0f, a);
                yield return 0;
            }
        }

        IEnumerator FadeOut(string scene)
        {
            float t = 0f;

            while (t < 1f)
            {
                t += Time.deltaTime;
                float a = curve.Evaluate(t);
                img.color = new Color(0f, 0f, 0f, a);
                yield return 0;
            }

            SceneManager.LoadScene(scene);
        }
        IEnumerator FadeOutWithAddCoin(string scene)
        {
            float t = 0f;

            while (t < 1f)
            {
                t += Time.deltaTime;
                float a = curve.Evaluate(t);
                img.color = new Color(0f, 0f, 0f, a);
                yield return 0;
            }
            coinWon += currentCoins;
            PlayerPrefs.SetInt(scriptShop.coinsKey, coinWon);
            PlayerPrefs.Save();
            SceneManager.LoadScene(scene);
        }


    }
}