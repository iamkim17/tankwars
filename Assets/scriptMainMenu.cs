using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class scriptMainMenu : MonoBehaviour {

    //Load level play
    public void Play(string loadLevel)
    {
        SceneManager.LoadScene(loadLevel);
    }
    //Load level settings
    public void Settings(string loadLevel)
    {
        SceneManager.LoadScene(loadLevel);
    }
    //Load level Shop
    public void Shop(string loadLevel)
    {
        SceneManager.LoadScene(loadLevel);
    }
    //Load Application Exit
    public void ApplicationExit()
    {
        Application.Quit();
    }


}
