using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class GameSoundManager : MonoBehaviour {
    public Toggle toggle;
    public int backgroundMusic;
    string key = "bgMusic10";
    // Use this for initialization
    private void Awake()
    {
        toggle = GameObject.FindWithTag("tglBgMusic").GetComponent<Toggle>() as Toggle;
        //PlayerPrefs.GetInt("bgMusic", bgMusic);
        //backgroundMusic = PlayerPrefs.GetInt(key);

    }
    void Start () {
        
    }
    private void Update()
    {
        backgroundMusic = PlayerPrefs.GetInt(key);
        Debug.Log(PlayerPrefs.GetInt(key).ToString());
        if (backgroundMusic == 1)
        {
            toggle.isOn = true;
        }
        else
        {
            toggle.isOn = false;
        }
    }

    public void toggleBackgroundMusic()
    {


        if (toggle.isOn)
        {
            backgroundMusic = 1;
            AudioListener.volume = 1;
            PlayerPrefs.SetInt(key, backgroundMusic);

        }
        else
        {
            backgroundMusic = 0;
            AudioListener.volume = 0;
            PlayerPrefs.SetInt(key, backgroundMusic);
        }
            

        Debug.Log(backgroundMusic);
        
    }
}
