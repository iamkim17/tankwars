using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class scriptCameraShake : MonoBehaviour {

    public Camera mainCam;

    float shakeAmount = 0;

    private void Awake()
    {
        //if our mainCam is null then set our Main Camera to mainCam variable.
        if (mainCam == null)
            mainCam = Camera.main;
    }

    //Shake method in our camera when called. 
    //Invoking the BeginShake when Shake method called.
    //Shake depends in _lenght.
    public void Shake(float _amount, float _length)
    {
        shakeAmount = _amount;
        InvokeRepeating("BeginShake", 0, 0.01f);
        Invoke("StopShake", _length);
    }
    //When shake method called, this will shake the camera depends on our offset X and Y.
    //then translate the position every Invoke Repeat.
    void BeginShake()
    {
        if(shakeAmount > 0)
        {
            Vector3 camPos = mainCam.transform.position;

            float offsetX = Random.value * shakeAmount * 2 - shakeAmount;
            float offsetY = Random.value * shakeAmount * 2 - shakeAmount;

            camPos.x += offsetX;
            camPos.y += offsetY;

            mainCam.transform.position = camPos;

        }
    }
    //if the _lenght of shake is done then stop the Shaking by CancelInvoke.
    //translating the position of the camera to zero.
    void StopShake()
    {
        CancelInvoke("BeginShake");

        mainCam.transform.localPosition = Vector3.zero;
    }
}
