using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class scriptDamageCollider : MonoBehaviour {

    public int currentEnemyHealth;          //Enemy Health
    public int maxHealth = 100;             //given max health

    private void Start()
    {
        //our current enemy health is equal to the max health which is 100.
        currentEnemyHealth = maxHealth;
    }
    private void Update()
    {
        //Check if our currentEnemyHealth is greather than 0 then destroy enemy game object.
        if (currentEnemyHealth <= 0)
        {
            EnemyDestroy();
        }
    }

    //when collision triggered with PlayerBullet then Send data to TakeEnemyDamage dependes on scriptTurret.hitDamage value.
    private void OnTriggerEnter2D(Collider2D collision)
    {
        if(collision.CompareTag("PlayerBullet"))
        {
            TakeEnemyDamage(scriptTurret.hitDamage);
        }
    }
    
    //EnemyDestryo Method will called when currentEnemyhealth is equal to 0.
    //Tank Counter is  -1 text and set our points counter to + 1
    void EnemyDestroy()
    {
        Destroy(this.gameObject);
        scriptUIManager.Instance.GetSetTankCounter -= 1;
        scriptUIManager.Instance.GetSetPointCounuter += 1;
    }

    //TakeEnemyDamage called when Enemy Colided with the Player Bullet.
    //currentEnemyHealth will minus to the amount damage.
    public void TakeEnemyDamage(int _amount)
    {
        currentEnemyHealth -= _amount;
    }
}
