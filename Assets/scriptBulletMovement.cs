using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class scriptBulletMovement : MonoBehaviour {
    public float speed;
    public float destroy;
    public float enemyDamage;
    public int playerDamage;
    //public GameObject enemyTarget;
    public GameObject target;
    public bool isBulletDestroy;

    public bool GetSetIsBulletDestroy
    {
        get { return isBulletDestroy; }
        set { isBulletDestroy = value; }
    }

    // Use this for initialization
    void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
        //Bullet direction
        transform.Translate(Vector3.up * speed * Time.deltaTime);
        Destroy(gameObject, destroy);
    }

    //if Collision is triggered then compare the tag then destroy
    //or send damage to the enemy.
    private void OnTriggerEnter2D(Collider2D collision)
    {
        if(collision.CompareTag("EnemyTank"))
        {
            Destroy(gameObject);
        }
        if (collision.CompareTag("Obstacles"))
        {
            Destroy(gameObject);
        }
        if (collision.CompareTag("PlayerTank"))
        {
            Destroy(gameObject);
            target.SendMessage("TakePlayerDamage", enemyDamage);
        }

    }
}
