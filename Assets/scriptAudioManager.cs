using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class scriptAudioManager : MonoBehaviour {
    public string[] Gameplays;
    static bool AudioBegin = false;
    int bgMusic;
    private void Awake()
    {

        //Get bgMusic value in our PlayerPref named bgMusic10
        // if the AudioBegin is false, Get Component of our AudioSource then play it
        //to be able to play it while changing scene we need to initialize
        //DontDestroyOnLoad
        bgMusic = PlayerPrefs.GetInt("bgMusic10");
        if (!AudioBegin)
        {
            GetComponent<AudioSource>().Play();
            DontDestroyOnLoad(gameObject);
            AudioBegin = true;
            

        }
    }
    // Use this for initialization
    void Start () {
        //if bgMusic value is 1 then volume is 1
        //else our volume will be 0
        if (bgMusic == 1)
        {
            AudioListener.volume = 1;
        }
        else
        {
            AudioListener.volume = 0;
        }
    }
	
	// Update is called once per frame
	void Update () {
        //Loop name of level then if the loaded level is equals to the array of string
        // then stop the Audio.
        for (int i = 0; i < Gameplays.Length; i++)
        {
            if (Application.loadedLevelName == Gameplays[i])
            {
                GetComponent<AudioSource>().Stop();
                AudioBegin = false;
            }
        }
    }
}
