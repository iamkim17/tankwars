using UnityEngine;

[System.Serializable]
public class scriptColorToPrefab {

    public Color color;
    public GameObject prefab;
}
