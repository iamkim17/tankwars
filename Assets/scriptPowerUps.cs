using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class scriptPowerUps : MonoBehaviour {

    public powerUps instancePowerUps;
    scriptPowerUps thePowerUps;
    public scriptTank playerTank;
    
    /// <summary>
    /// Enum for what kind of PowerUps
    /// </summary>
    public enum powerUps
    {
        healthPowerUp,
        defensePowerUp,
        inviPowerup
    };

    /// <summary>
    /// Getter Setter for Power ups
    /// </summary>
    public powerUps GetSetPowerUps
    {
        get { return instancePowerUps; }
        set { instancePowerUps = value; }
    }

    private void Awake()
    {
        //Getting the component of scriptPowerUps and assign it to thePowerUps
        thePowerUps = GetComponent<scriptPowerUps>();
        //Finding playerTank and assign it to playerTank
        playerTank = GameObject.FindGameObjectWithTag("PlayerTank").GetComponent<scriptTank>();
    }
    private void OnTriggerEnter2D(Collider2D collision)
    {
        //if player collided with healthPowerUp
        //check if current health is greater than max health then set current health to max health
        //to prevent from over exceeding the player health
        //else plus 15 to current health then destroy power up
        if(thePowerUps.GetSetPowerUps == scriptPowerUps.powerUps.healthPowerUp)
        {
            if (collision.CompareTag("PlayerTank"))
            {
                if(playerTank.curHealth >= playerTank.maxHealth)
                {
                    playerTank.curHealth = playerTank.maxHealth;
                }
                else
                {
                    playerTank.curHealth += 15;
                }
                
                Destroy(this.gameObject);
                
            }
        }
        //if player collided with defensePowerUp
        //player thank defense mechanism on, shield activated with stacking timer
        //then destroy object
        if (thePowerUps.GetSetPowerUps == scriptPowerUps.powerUps.defensePowerUp)
        {
            if (collision.CompareTag("PlayerTank"))
            {
                playerTank.isDefPowerUpOn = true;
                playerTank.shield.SetActive(true);
                playerTank.shieldTimer += 5f;
                Destroy(this.gameObject);
            }
        }
        //if player collided with inviPowerup
        //invisible mechanism with stacking timer
        //then destroy object
        if (thePowerUps.GetSetPowerUps == scriptPowerUps.powerUps.inviPowerup)
        {
            if (collision.CompareTag("PlayerTank"))
            {
                scriptTank.isInviPowerUpOn = true;
                playerTank.inviTimer += 5f;
                Destroy(this.gameObject);
            }
        }
    }
}
