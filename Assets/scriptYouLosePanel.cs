using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;
public class scriptYouLosePanel : MonoBehaviour {

    public TextMeshProUGUI textCoinsPrize;
    public TextMeshProUGUI textCurrentCoins;

    int currentCoins;
    private void Awake()
    {
        currentCoins = PlayerPrefs.GetInt(scriptShop.coinsKey); // get current coin
    }

    private void Update()
    {
        //Display text in Lose Panel
        textCoinsPrize.text = scriptUIManager.Instance.GetSetPointCounuter.ToString();
        textCurrentCoins.text = currentCoins.ToString();
    }
}
