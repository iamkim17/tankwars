using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;
public class scriptUIManager : MonoSingleton<scriptUIManager> {
    /// <summary>
    /// This is script is a singleton method so that you can access globally all its variable and methods in entire script
    /// like getter and setter, for better Readability of the script.
    /// </summary>
    [SerializeField]
    Text _points;
    [SerializeField]
    Text _tankCount;
    [SerializeField]
    int _tankCounter;
    [SerializeField]
    int _pointsCounter;
    
    public GameObject backPanel;

    void Awake()
    {
        Instance = this;
        backPanel.SetActive(false);
    }

    public Text GetPoints
    {
        get { return _points; }
    }

    public Text GetTankCount
    {
        get { return _tankCount; }
    }

    public int GetSetTankCounter
    {
        get { return _tankCounter; }
        set { _tankCounter = value; }
    }

    public int GetSetPointCounuter
    {
        get { return _pointsCounter; }
        set { _pointsCounter = value; }
    }

    private void Update()
    {
        UITextField();
    }
    void UITextField()
    {
        _tankCount.text = _tankCounter.ToString() + " /20";
        _points.text = _pointsCounter.ToString();
    }

    public void PauseGame(GameObject _backPanel)
    {
        Time.timeScale = 0;
        backPanel = _backPanel;
        backPanel.SetActive(true);
    }
    public void UnPauseGame(GameObject _backPanel)
    {
        Time.timeScale = 1;
        backPanel = _backPanel;
        backPanel.SetActive(false);
    }

    public void BackToMainMenu(string LoadLevel)
    {
        SceneManager.LoadScene(LoadLevel);
        Time.timeScale = 1;
    }
}
