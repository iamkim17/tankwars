using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using CnControls;

public class Movement : MonoBehaviour
{
	
	public float speed = 200f;
	//public float turretSpeed = 100f;
	bool move = false;
	public void Move() {
		move = true;
	}
	public void Unmove() {
		move = false;
	}
	void FixedUpdate()
	{
		/*var x = CnInputManager.GetAxis("Horizontal") * Time.deltaTime * speed;
		var y = CnInputManager.GetAxis("Vertical") * Time.deltaTime * speed;
		var t = CnInputManager.GetAxis("Rotate") * Time.deltaTime * turretSpeed;*/

		var x = CnInputManager.GetAxis("Horizontal") * Time.deltaTime;
		var y = CnInputManager.GetAxis("Vertical") * Time.deltaTime;
		//var t = CnInputManager.GetAxis("Rotate") * Time.deltaTime;

		//transform.Rotate(0, 0, t);
		//transform.Translate(x, y, 0);
		Vector3 movement = new Vector3(x, y, 0);
		if(move) {
			float rot_z = Mathf.Atan2(y, x) * Mathf.Rad2Deg;
			transform.rotation = Quaternion.Euler(0f, 0f, rot_z - 90);
		}


		transform.Translate (movement * speed * Time.deltaTime, Space.World);
	}

}
