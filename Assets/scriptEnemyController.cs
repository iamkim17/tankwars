using System.Collections;
using System.Collections.Generic;
using UnityEngine;


public class scriptEnemyController : MonoBehaviour {

    public Transform target;
    public AIPath path;
    float distance;
    public float chaseRange = 15f;
    public int currentEnemyHealth;
    public int maxHealth = 100;
    public float stopChase = 3f;
    public int coinPoints;
    
    private void Awake()
    {
        //Library for AI. Getting AIpath script from game object component
        path = GetComponent<AIPath>();
        //get PlayerTank transform and assign it to target.
        target = GameObject.FindGameObjectWithTag("PlayerTank").transform;
        
    }

    private void Start()
    {
        currentEnemyHealth = maxHealth;
    }

    private void Update()
    {
        //calculate distance from target position from current transform position
        distance = Vector2.Distance(target.position, transform.position);
        //if distance greater than equal to stopChase
        //calculate path distance by allowing AI script to move.
        //else if the invisible power is on in our player
        //enemy can't see the target player so enemy can't move and enemy can't shoot
        //then if the distance less than chaseRange AI can move and hoost
        if(distance <= stopChase)
        {
            path.canMove = false;
        }
        else
        {
            if (scriptTank.isInviPowerUpOn == true)
            {
                path.canMove = false;
                scriptEnemyAim.canShoot = false;
            }
            else
            {
                if (distance < chaseRange)
                {

                    path.canMove = true;
                    scriptEnemyAim.canShoot = true;
                }
                else
                {

                    path.canMove = false;
                }
            }

            
        }
        //enemy current health is less than 0 enemy will be destroyed.
        if (currentEnemyHealth <= 0)
        {
            EnemyDestroy();
        }

        
    }

    //if Collision is triggered then compare the tag then destroy
    //or send damage to the enemy.
    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.CompareTag("PlayerBullet"))
        {
            TakeEnemyDamage(scriptTurret.hitDamage);
        }
    }
    //Take damage from player depends on amount
    public void TakeEnemyDamage(int _amount)
    {
        currentEnemyHealth -= _amount;
    }

    //enemy destroy counter remaining in tank is - 1;
    //enemy points counter + coinPoints;
    void EnemyDestroy()
    {
        Destroy(this.gameObject);
        scriptUIManager.Instance.GetSetTankCounter -= 1;
        scriptUIManager.Instance.GetSetPointCounuter += coinPoints;
    }



}
