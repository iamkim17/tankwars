﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class scriptTank : MonoBehaviour {

    public float speed = 200f;
    //public float turretSpeed = 100f;
    bool move = false;
    public void Move()
    {
        move = true;
    }
    public void Unmove()
    {
        move = false;
    }
    void FixedUpdate()
    {

        var x = Input.GetAxis("Horizontal") * Time.deltaTime;
        var y = Input.GetAxis("Vertical") * Time.deltaTime;

        if(x > 0.1)
        {
            move = true;
        }
        else
        {
            move = false;
        }

        if (x < -0.1)
        {
            move = true;
        }
        else
        {
            move = false;
        }

        if (y > 0.1)
        {
            move = true;
        }
        else
        {
            move = false;
        }

        if (y < -0.1)
        {
            move = true;
        }
        else
        {
            move = false;
        }

        Vector3 movement = new Vector3(x, y, 0);
        if (move)
        {
            float rot_z = Mathf.Atan2(y, x) * Mathf.Rad2Deg;
            transform.rotation = Quaternion.Euler(0f, 0f, rot_z - 90);
        }


        transform.Translate(movement * speed * Time.deltaTime, Space.World);
    }
}
