﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class scriptTank : MonoBehaviour {

    public float tankMovementSpeed;
    public float tankRotationSpeed;
	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void FixedUpdate () {
        float translation = ControlFreak2.CF2Input.GetAxis("Vertical") * tankMovementSpeed;
        float rotation = ControlFreak2.CF2Input.GetAxis("Horizontal") * tankRotationSpeed;

        transform.Translate(0, -translation * Time.deltaTime, 0);
        transform.Rotate(0,0, -rotation * Time.deltaTime);
	}
}
