﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class sciprtTurret : MonoBehaviour {

    public float turretRotationSpeed;
    // Use this for initialization
    void Start () {
		
	}
	
	// Update is called once per frame
	void FixedUpdate () {
        float rotation = ControlFreak2.CF2Input.GetAxis("HorizontalTurret") * turretRotationSpeed;
        transform.Rotate(0, 0, -rotation * Time.deltaTime);
    }
}
